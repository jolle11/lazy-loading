import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../models/core.model';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
    public menuList: MenuItem[] = [];
    constructor() {}

    ngOnInit(): void {
        this.menuList = [
            {
                label: 'Home',
                url: '/home',
            },
            {
                label: 'Admin',
                url: '/admin',
            },
            {
                label: 'Shoes',
                url: '/shoes',
            },
        ];
    }
}
