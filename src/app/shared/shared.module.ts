import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedComponent } from './shared.component';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
    declarations: [SharedComponent, LoaderComponent],
    imports: [CommonModule],
    exports: [SharedComponent, LoaderComponent],
})
export class SharedModule {}
