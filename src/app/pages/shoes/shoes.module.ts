import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShoesRoutingModule } from './shoes-routing.module';
import { ShoesComponent } from './shoes.component';
import { ShoeDetailComponent } from './components/shoe-detail/shoe-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [ShoesComponent, ShoeDetailComponent],
    imports: [CommonModule, ShoesRoutingModule, SharedModule],
})
export class ShoesModule {}
