import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShoesComponent } from './shoes.component';
import { ShoeDetailComponent } from './components/shoe-detail/shoe-detail.component';

const routes: Routes = [
    {
        path: '',
        component: ShoesComponent,
    },
    {
        path: ':id',
        component: ShoeDetailComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ShoesRoutingModule {}
